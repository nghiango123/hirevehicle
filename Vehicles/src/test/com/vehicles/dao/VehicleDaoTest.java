package com.vehicles.dao;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.vehicles.model.Vehicle;

class VehicleDaoTest {

	private DriverManagerDataSource dataSource;
	private IVehicleDao vehicleDao;
	
	public VehicleDaoTest() {
		this.dataSource = new DriverManagerDataSource();
		this.dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
		this.dataSource.setUrl("jdbc:mysql://localhost:3306/hirevehicle");
		this.dataSource.setUsername("root");
		this.dataSource.setPassword("1234");
		this.vehicleDao = new VehicleDao(dataSource);
	}
	@Test
	void testGet() {
		Vehicle vehicle = vehicleDao.get(1);
		
		if (vehicle != null) {
			System.out.print(vehicle);
		}
		
		assertNotNull(vehicle);
	}

}
