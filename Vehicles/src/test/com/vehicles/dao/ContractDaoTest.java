package com.vehicles.dao;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.vehicles.model.Contract;

class ContractDaoTest {
	private DriverManagerDataSource dataSource;
	private IContractDao contractDao;

	
	public ContractDaoTest() {
		this.dataSource = new DriverManagerDataSource();
		this.dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
		this.dataSource.setUrl("jdbc:mysql://localhost:3306/hirevehicle");
		this.dataSource.setUsername("root");
		this.dataSource.setPassword("1234");
		this.contractDao = new ContractDao(dataSource);
	}
	@Test
	void testGetAll() {
		List<Contract> listContract = contractDao.getAll();
		
		for (Contract con : listContract) {
			System.out.print(con);
		}
		
		assertTrue(!listContract.isEmpty());
	}
	
	@Test
	void testGet() {
		Contract contract = contractDao.get(1);
		if (contract != null) {
			System.out.println(contract.getCode());
			System.out.println(contract.getCodeUser().getName());
			System.out.println(contract.getCodeVehicle().getName());
			System.out.println(contract.getAsset());
		}
		
		assertNotNull(contract);
	}
	@Test 
	void testUpdate() {
		Integer result = contractDao.update(2);
		
		assertTrue(result > 0);
	}

}
