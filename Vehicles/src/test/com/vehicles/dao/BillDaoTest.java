package com.vehicles.dao;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.SystemMenuBar;

import org.junit.jupiter.api.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.vehicles.mapper.ContractRowMapper;
import com.vehicles.mapper.StaffRowMapper;
import com.vehicles.mapper.UserRowMapper;
import com.vehicles.model.Bill;
import com.vehicles.model.Contract;
import com.vehicles.model.Staff;
import com.vehicles.model.User;

class BillDaoTest {
	private DriverManagerDataSource dataSource;
	private IBillDao billDao;
	private JdbcTemplate jdbcTemplate;

	
	public BillDaoTest() {
		this.dataSource = new DriverManagerDataSource();
		this.dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
		this.dataSource.setUrl("jdbc:mysql://localhost:3306/hirevehicle");
		this.dataSource.setUsername("root");
		this.dataSource.setPassword("1234");
		this.billDao = new BillDao(dataSource);
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Test
	void testSave() {
		String sqlStaff = "select * from staff where code="+1;
		Staff staff = jdbcTemplate.query(sqlStaff, new StaffRowMapper(jdbcTemplate));
		String sqlContract = "select * from contract where code="+1;
		Contract contract = jdbcTemplate.query(sqlContract, new ContractRowMapper(jdbcTemplate));
		Date time = Date.valueOf(LocalDate.now());
		
		Bill bill = new Bill(100.0, 200.0, "Oke on", 100.1, time, contract, staff);
		
		int result = billDao.save(bill);
		
		assertTrue(result > 0);
		
	}
	
	@Test
	void testGet() {
		Bill bill = billDao.get(1);
		if (bill != null) {
			System.out.println(bill.getCodeContract() + " " + bill.getCodeStaff());
		}
		assertNotNull(bill);
	}
	
	@Test
	void testGetAll() {
		List<Bill> bills = billDao.getAll();
		System.out.println(bills.size());
		
		for (Bill bill : bills) {
			System.out.println(bill.getCode() + " " + bill.getListBroken());
			System.out.println(bill);
			System.out.println(bill.getCodeStaff());
			System.out.println(bill.getCodeContract());
		}
		assertTrue(!bills.isEmpty());
	}

}
