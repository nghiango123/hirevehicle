package com.vehicles.dao;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.vehicles.model.User;

class UserDaoTest {
	private DriverManagerDataSource dataSource;
	private IUserDao userDao;
	
	public UserDaoTest() {
		this.dataSource = new DriverManagerDataSource();
		this.dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
		this.dataSource.setUrl("jdbc:mysql://localhost:3306/hirevehicle");
		this.dataSource.setUsername("root");
		this.dataSource.setPassword("1234");
		this.userDao = new UserDao(dataSource);
	}
	
	@Test
	void testGet() {
		User user = userDao.get(1);
		if (user != null) {
			System.out.println(user.getName());
			System.out.println(user.getCode());
		}
		System.out.println(user);
		
		assertNotNull(user);
	}

	@Test
	void testGetAll() {
		System.out.print(LocalDate.now());
	}

//	@Test
//	void testSave() {
//		User user = new User("Nghia ngo", true, "Ha Noi", "0383982930", "Rat tot");
//		int result = userDao.save(user);
//		assertTrue(result > 0);
//	}

}
