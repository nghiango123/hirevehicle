package com.vehicles.dao;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.vehicles.model.Staff;

class StaffDaoTest {
	private DriverManagerDataSource dataSource;
	private IStaffDao staffDao;
	private JdbcTemplate jdbcTemplate;

	
	public StaffDaoTest() {
		this.dataSource = new DriverManagerDataSource();
		this.dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
		this.dataSource.setUrl("jdbc:mysql://localhost:3306/hirevehicle");
		this.dataSource.setUsername("root");
		this.dataSource.setPassword("1234");
		this.staffDao = new StaffDao(dataSource);
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Test
	void testGet() {
		fail("Not yet implemented");
	}

	@Test
	void testGetAll() {
		List<Staff> staffs = staffDao.getAll();
		
		for (Staff con : staffs) {
			System.out.println(con);
		}
		
		assertTrue(!staffs.isEmpty());
	}

}
