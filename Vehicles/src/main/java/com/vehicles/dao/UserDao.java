package com.vehicles.dao;

import java.util.List;
import java.util.Optional;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;

import com.vehicles.mapper.UserRowMapper;
import com.vehicles.model.User;


public class UserDao implements IUserDao {
	private JdbcTemplate jdbcTemplate;
	
	public UserDao(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}


	@Override
	public User get(long id) {
		String sql = "select * from user where code="+id;
		return jdbcTemplate.query(sql, new UserRowMapper(jdbcTemplate));
	}

	@Override
	public List<User> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int save(User user) {
		String sql = "insert into user (name, is_cooperator, address, phone, note) values (?, ?, ?, ?, ?, ?)";
		return jdbcTemplate.update(sql, user.getName(), user.getIsCooperator(), user.getAddress(), user.getPhone(), user.getNote());
	}


	@Override
	public void update(User user, String[] params) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void delete(User user) {
		// TODO Auto-generated method stub
		
	}



}
