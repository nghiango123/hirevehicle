package com.vehicles.dao;

import java.util.List;
import java.util.Optional;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.vehicles.mapper.VehicleRowMapper;
import com.vehicles.model.Vehicle;

public class VehicleDao implements IVehicleDao {

	private JdbcTemplate jdbcTemplate;
	
	public VehicleDao(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	@Override
	public Vehicle get(long id) {
		
		String sql = "select * from vehicle where code = "+id;
		
		return jdbcTemplate.query(sql,  new VehicleRowMapper(jdbcTemplate));
		
	}

	@Override
	public List<Vehicle> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int save(Vehicle vehicle) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void update(Vehicle vehicle) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Vehicle vehicle) {
		// TODO Auto-generated method stub
		
	}

}
