package com.vehicles.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.vehicles.mapper.ContractRowMapper;
import com.vehicles.mapper.StaffRowMapper;
import com.vehicles.mapper.UserRowMapper;
import com.vehicles.mapper.VehicleRowMapper;
import com.vehicles.model.Contract;
import com.vehicles.model.Staff;
import com.vehicles.model.User;
import com.vehicles.model.Vehicle;

public class StaffDao implements IStaffDao {
	
	private JdbcTemplate jdbcTemplate;
	
	public StaffDao(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public Staff get(long id) {
		String sql = "select * from staff where code="+id;
		return jdbcTemplate.query(sql, new StaffRowMapper(jdbcTemplate));
	}

	@Override
	public List<Staff> getAll() {
		String sql = "select * from staff";
		List<Staff> staffs = jdbcTemplate.query(sql, 
	        new ResultSetExtractor<List<Staff>>(){	         
			@Override
			public List<Staff> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<Staff> list = new ArrayList<Staff>();
				while(rs.next()) {
					Integer code = rs.getInt("code");
					String name = rs.getString("name");
					String address = rs.getString("address");
					String phone = rs.getString("phone");
					String position = rs.getString("position");
					String note = rs.getString("note");
					
					list.add(new Staff(code, name, address, phone, position, note));
				}
				
				return list;
			}   
		});		
		return staffs;
	}

	@Override
	public int save(Staff staff) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void update(Staff staff) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Staff staff) {
		// TODO Auto-generated method stub
		
	}

}
