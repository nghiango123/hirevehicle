package com.vehicles.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.vehicles.mapper.BillRowMapper;
import com.vehicles.mapper.ContractRowMapper;
import com.vehicles.mapper.StaffRowMapper;
import com.vehicles.model.Bill;
import com.vehicles.model.Contract;
import com.vehicles.model.Staff;

public class BillDao implements IBillDao {

    private JdbcTemplate jdbcTemplate;
    
    public BillDao(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }
	@Override
	public Bill get(long id) {
		String sql = "select * from bill where code="+ id;
		
		return jdbcTemplate.query(sql, new BillRowMapper(jdbcTemplate));
	}

	@Override
	public List<Bill> getAll() {
		String sql = "select * from bill";
		List <Bill> bills = jdbcTemplate.query(sql, 
	        new ResultSetExtractor<List<Bill>>(){
	         
			@Override
			public List<Bill> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<Bill> list = new ArrayList<Bill>();
				while(rs.next()) {
					Integer code = rs.getInt("code");
					Double unit = rs.getDouble("unit");
					Double total = rs.getDouble("total");
					String listBroken = rs.getString("list_broken");
					Double compensation = rs.getDouble("compensation");
					Date datePay = rs.getDate("date_pay");
					
					Integer codeContract = rs.getInt("code_contract");
					Integer codeStaff = rs.getInt("code_staff");
					
					String sqlContract = "select * from contract where code="+ codeContract;
					String sqlStaff = "select * from staff where code="+ codeStaff;
					
					Contract contract = jdbcTemplate.query(sqlContract, new ContractRowMapper(jdbcTemplate));
					Staff staff = jdbcTemplate.query(sqlStaff, new StaffRowMapper(jdbcTemplate));
					
					list.add(new Bill(code, unit, total, listBroken, compensation, datePay, contract, staff));
				}
				
				return list;
			}    	  
	    });
		return bills;

	}

	@Override
	public int save(Bill bill) {
		String sql = "insert into bill (unit, total, list_broken, compensation, date_pay,  code_contract, code_staff)"
				+ "values (?, ?, ?, ?, ?, ?, ?)";
		return jdbcTemplate.update(sql, bill.getUnit(), bill.getTotal(), bill.getListBroken(), bill.getCompensation(), bill.getDatePay(), bill.getIdContract(), bill.getIdStaff());
	}
	
	@Override
	public int update(Bill bill) {
		String sql = "update bill set date_pay = ?, total= ? where code= ?";
		
		return jdbcTemplate.update(sql, bill.getDatePay(), bill.getTotal(), bill.getCode());
		
	}

	@Override
	public void delete(Bill bill) {
		// TODO Auto-generated method stub
		
	}

}
