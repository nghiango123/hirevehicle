package com.vehicles.dao;

import java.util.List;
import java.util.Optional;

import com.vehicles.model.Bill;

public interface IBillDao {
	Bill get(long id);
    
    List<Bill> getAll();
    
    int save(Bill bill);
    
    int update(Bill bill);
    
    void delete(Bill bill);
}
