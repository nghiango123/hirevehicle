package com.vehicles.dao;

import java.util.List;
import java.util.Optional;

import com.vehicles.model.Contract;

public interface IContractDao {
	Contract get(long id);
    
    List<Contract> getAll();
    
    int save(Contract contract);
    
    int update(long id);
    
    
    void delete(Contract contract);
    
    List<Contract> getContractNotPay();
}
