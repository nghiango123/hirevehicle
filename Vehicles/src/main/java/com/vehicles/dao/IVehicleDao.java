package com.vehicles.dao;

import java.util.List;
import java.util.Optional;

import com.vehicles.model.Vehicle;

public interface IVehicleDao {
	Vehicle get(long id);
    
    List<Vehicle> getAll();
    
    int save(Vehicle vehicle);
    
    void update(Vehicle vehicle);
    
    void delete(Vehicle vehicle);
}
