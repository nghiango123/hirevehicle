package com.vehicles.dao;

import java.util.List;
import java.util.Optional;

import com.vehicles.model.Staff;

public interface IStaffDao {
	Staff get(long id);
    
    List<Staff> getAll();
    
    int save(Staff staff);
    
    void update(Staff staff);
    
    void delete(Staff staff);
}
