package com.vehicles.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.sql.DataSource;

import com.vehicles.mapper.ContractRowMapper;
import com.vehicles.mapper.StaffRowMapper;
import com.vehicles.mapper.UserRowMapper;
import com.vehicles.mapper.VehicleRowMapper;
import com.vehicles.model.Bill;
import com.vehicles.model.Contract;
import com.vehicles.model.Staff;
import com.vehicles.model.User;
import com.vehicles.model.Vehicle;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

public class ContractDao implements IContractDao {
	
    private JdbcTemplate jdbcTemplate;
    
    public ContractDao(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

	@Override
	public Contract get(long id) {
		String sql = "select * from contract where code="+ id;
		return jdbcTemplate.query(sql, new ContractRowMapper(jdbcTemplate));
	}


	@Override
	public List<Contract> getAll() {
		String sql = "select * from contract";
		List<Contract> contracts = jdbcTemplate.query(sql, 
	        new ResultSetExtractor<List<Contract>>(){
	         
			@Override
			public List<Contract> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<Contract> list = new ArrayList<Contract>();
				while(rs.next()) {
					Integer code = rs.getInt("code");
					Date rentalDate = rs.getDate("rental_date");
					Date returnDate = rs.getDate("return_date");
					Double deposit = rs.getDouble("deposit");
					Boolean status = rs.getBoolean("status");
					String typeContract = rs.getString("type_contract");
					String asset = rs.getString("asset");
					
					Integer codeUser = rs.getInt("code_user");
					Integer codeVehicle = rs.getInt("code_vehicle");
						
					String sqlUser = "select * from user where code="+codeUser;
					String sqlVehicle = "select * from vehicle where code="+codeVehicle;
					
					User user = jdbcTemplate.query(sqlUser, new UserRowMapper(jdbcTemplate));
					Vehicle vehicle = jdbcTemplate.query(sqlVehicle, new VehicleRowMapper(jdbcTemplate));
					
					
					list.add(new Contract(code, rentalDate, returnDate, deposit, status, typeContract, asset, user, vehicle));
				}
				
				return list;
			}   
		});
		
		return contracts;
	}

	@Override
	public int save(Contract contract) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int update(long id) {
		String sql = "update contract set status=true where code =" + id;
		return jdbcTemplate.update(sql);
	}
	@Override
	public void delete(Contract contract) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Contract> getContractNotPay() {
		String sql = "select * from contract where status=false";
		List<Contract> contracts = jdbcTemplate.query(sql, 
		        new ResultSetExtractor<List<Contract>>(){
		         
				@Override
				public List<Contract> extractData(ResultSet rs) throws SQLException, DataAccessException {
					List<Contract> list = new ArrayList<Contract>();
					while(rs.next()) {
						Integer code = rs.getInt("code");
						Date rentalDate = rs.getDate("rental_date");
						Date returnDate = rs.getDate("return_date");
						Double deposit = rs.getDouble("deposit");
						Boolean status = rs.getBoolean("status");
						String typeContract = rs.getString("type_contract");
						String asset = rs.getString("asset");
						
						Integer codeUser = rs.getInt("code_user");
						Integer codeVehicle = rs.getInt("code_vehicle");
							
						String sqlUser = "select * from user where code="+codeUser;
						String sqlVehicle = "select * from vehicle where code="+codeVehicle;
						
						User user = jdbcTemplate.query(sqlUser, new UserRowMapper(jdbcTemplate));
						Vehicle vehicle = jdbcTemplate.query(sqlVehicle, new VehicleRowMapper(jdbcTemplate));
						
						
						list.add(new Contract(code, rentalDate, returnDate, deposit, status, typeContract, asset, user, vehicle));
					}
					
					return list;
				}   
			});
			
			return contracts;
		
	}





}
