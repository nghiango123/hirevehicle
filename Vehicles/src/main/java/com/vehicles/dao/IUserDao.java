package com.vehicles.dao;

import java.util.List;
import java.util.Optional;

import com.vehicles.model.User;

public interface IUserDao{
	User get(long id);
    
    List<User> getAll();
    
    int save(User user);
    
    void update(User user, String[] params);
    
    void delete(User user);
}
