package com.vehicles.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.vehicles.dao.*;
import com.vehicles.model.*;

@Controller
public class ContractController {
	@Autowired
	private IContractDao contractDao;
	
	@Autowired
	private IUserDao userDao;
	@RequestMapping(value="contract", method=RequestMethod.GET)
	public ModelAndView listContract() {
		var mav = new ModelAndView();
		List<Contract> contracts = contractDao.getContractNotPay();
		mav.addObject("contracts", contracts);
		mav.setViewName("contract");
		return mav;
	}
	
	@RequestMapping(value="contractall",  method = RequestMethod.GET)
	public ModelAndView listContractall() {
		var mav = new ModelAndView();
		List<Contract> contracts = contractDao.getAll();
		mav.addObject("contracts", contracts);
		mav.setViewName("contractall");
		return mav;
	}
}
