package com.vehicles.controller;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.vehicles.dao.*;
import com.vehicles.model.*;

@Controller
public class HomeController {
	
	@Autowired
	private IUserDao userDao;
	@Autowired
	private IContractDao contractDao;
	@Autowired
	private IBillDao billDao;
	
	@Autowired
	private IStaffDao staffDao;
	
	
	@RequestMapping(value="/",  method = RequestMethod.GET)
	public ModelAndView listUser() {
		var mav = new ModelAndView();
		Bill bill = billDao.get(1);
		Contract contract = contractDao.get(1);
		User user = userDao.get(1);
		Staff staff = staffDao.get(1);
		mav.addObject("user", user);
		mav.addObject("name", user.getName());
		mav.addObject("bill", bill);
		mav.addObject("contract", contract);
		mav.addObject("staff", staff);
		mav.setViewName("index");
		return mav;
	}
	
}
