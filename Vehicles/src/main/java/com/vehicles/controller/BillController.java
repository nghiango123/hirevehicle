package com.vehicles.controller;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DeferredImportSelector.Group;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.vehicles.dao.*;
import com.vehicles.model.*;

@Controller
public class BillController {
	@Autowired
	private IBillDao billDao;
	
	@Autowired
	private IContractDao contractDao;
	
	@Autowired
	private IStaffDao staffDao;

	
	@RequestMapping(value="bill",  method = RequestMethod.GET)
	public ModelAndView getBill(HttpServletRequest request) {
		var mav = new ModelAndView();
		Integer code = Integer.parseInt(request.getParameter("code"));
		Contract contract = contractDao.get(code);
		List<Staff> staffs = staffDao.getAll();
		mav.addObject("bill", new Bill());
		mav.addObject("contract", contract);
		mav.addObject("staffs", staffs);
		mav.setViewName("formbill");
		return mav;
	}
	
	@RequestMapping(value="billsave", method = RequestMethod.POST)
	public ModelAndView saveBill(@ModelAttribute("bill") Bill bill, ModelAndView mav, HttpServletRequest request) {
		Double tax = 0.01;
		Double total = 0.0;
		Double subTotal = 0.0;
		Integer codeContract = Integer.parseInt(request.getParameter("contractCode"));
		Integer codeStaff = Integer.parseInt(request.getParameter("staffCode"));
		Contract contract = contractDao.get(codeContract);
		Staff staff = staffDao.get(codeStaff);
		bill.setCodeContract(contract);
		bill.setCodeStaff(staff);
		
		
		String rentalDate = contract.getRentalDate().toString();
		String returnDate = contract.getReturnDate().toString();
		String[] list1 = rentalDate.split("-");
		String[] list2 = returnDate.split("-");
		Integer year_return, month_return, day_return, year_rental, month_rental, day_rental;
		year_rental = Integer.valueOf(list1[0]);
		year_return = Integer.valueOf(list2[0]);
		month_rental = Integer.valueOf(list1[1]);
		month_return = Integer.valueOf(list2[0]);
		day_rental = Integer.valueOf(list1[0]);
		day_return = Integer.valueOf(list2[0]);
		
		Integer time = (year_return*365 + month_return*30 + day_return) - (year_rental*365 + month_rental*30 + day_rental); 
		time = Math.abs(time);
		Double payHire = time * bill.getUnit();	
		subTotal = payHire + bill.getCompensation();
		total = subTotal - tax*subTotal;
		
		mav.addObject("time", year_rental);
		mav.addObject("subTotal", subTotal);
		mav.addObject("total", total);
		mav.addObject("tax", tax*subTotal);
		mav.addObject("payHire", payHire);
		mav.addObject("date", LocalDate.now());
		mav.addObject("bill", bill);
		mav.addObject("contract", bill.getCodeContract());
		mav.addObject("staff", bill.getCodeStaff());
		mav.setViewName("bill");
		bill.setTotal(total);
		bill.setDatePay(Date.valueOf(LocalDate.now()));
		billDao.save(bill);
		contractDao.update(codeContract);
		return mav;
	}
	
	@RequestMapping(value="billpay", method = RequestMethod.GET)
	public ModelAndView updateBill(ModelAndView mav, HttpServletRequest request) {
	//	Integer code = Integer.parseInt(request.getParameter("code"));
	//	contractDao.update(code);
		mav.setViewName("contract");
		return mav;
	}
	
}
