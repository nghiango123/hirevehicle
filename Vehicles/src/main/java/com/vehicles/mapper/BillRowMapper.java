package com.vehicles.mapper;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import com.vehicles.model.Bill;
import com.vehicles.model.Contract;
import com.vehicles.model.Staff;

public class BillRowMapper implements ResultSetExtractor<Bill> {
	
	private JdbcTemplate jdbcTemplate;
	public BillRowMapper(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
//	@Override
//	public Bill mapRow(ResultSet rs, int rowNum) throws SQLException {
//		Bill bill = new Bill();
//		bill.setCode(rs.getInt("code"));
//		bill.setUnit(rs.getDouble("unit"));
//		bill.setTotal(rs.getDouble("total"));
//		bill.setListBroken(rs.getString("list_broken"));
//		bill.setCompensation(rs.getDouble("compensation"));
//		Integer codeContract = rs.getInt("code_contract");
//		Integer codeStaff = rs.getInt("code_staff");
//		
//		String sqlContract = "select * from contract where code="+ codeContract;
//		String sqlStaff = "select * from staff where code="+ codeStaff;
//		
//		Contract contract = jdbcTemplate.queryForObject(sqlContract, new ContractRowMapper(jdbcTemplate));
//		bill.setCodeContract(contract);
//		
//		Staff staff = jdbcTemplate.queryForObject(sqlStaff, new StaffRowMapper(jdbcTemplate));
//		bill.setCodeStaff(staff);
//		
//		return bill;
//	}
	@Override
	public Bill extractData(ResultSet rs) throws SQLException, DataAccessException {
		// TODO Auto-generated method stub
		if (rs.next()) {
			Integer code = rs.getInt("code");
			Double unit = rs.getDouble("unit");
			Double total = rs.getDouble("total");
			String listBroken = rs.getString("list_broken");
			Double compensation = rs.getDouble("compensation");
			Date datePay = rs.getDate("date_pay");
			
			Integer codeContract = rs.getInt("code_contract");
			Integer codeStaff = rs.getInt("code_staff");
			
			String sqlContract = "select * from contract where code="+ codeContract;
			String sqlStaff = "select * from staff where code="+ codeStaff;
			
			Contract contract = jdbcTemplate.query(sqlContract, new ContractRowMapper(jdbcTemplate));
			Staff staff = jdbcTemplate.query(sqlStaff, new StaffRowMapper(jdbcTemplate));
			
			return new Bill(code, unit, total, listBroken, compensation, datePay, contract, staff);
		}
		return null;
	}

}
