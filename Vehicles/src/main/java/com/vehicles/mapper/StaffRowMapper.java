package com.vehicles.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import com.vehicles.model.Staff;

public class StaffRowMapper implements ResultSetExtractor<Staff> {

	private JdbcTemplate jdbcTemplate;
	public StaffRowMapper(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
//	@Override
//	public Staff mapRow(ResultSet rs, int rowNum) throws SQLException {
//		Staff staff = new Staff();
//		staff.setCode(rs.getInt("code"));
//		staff.setName(rs.getString("name"));
//		staff.setAddress(rs.getString("address"));
//		staff.setPhone(rs.getString("phone"));
//		staff.setPosition(rs.getString("position"));
//		staff.setNote(rs.getString("note"));
//		
//		return staff;
//	}
	@Override
	public Staff extractData(ResultSet rs) throws SQLException, DataAccessException {
		if (rs.next()) {
			Integer code = rs.getInt("code");
			String name = rs.getString("name");
			String address = rs.getString("address");
			String phone = rs.getString("phone");
			String position = rs.getString("position");
			String note = rs.getString("note");
			
			return new Staff(code, name, address, phone, position, note);
			
		}
		return null;
	}

}
