package com.vehicles.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import com.vehicles.model.User;
import com.vehicles.model.Vehicle;

public class VehicleRowMapper implements ResultSetExtractor<Vehicle> {

	private JdbcTemplate jdbcTemplate;
	public VehicleRowMapper(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
//	@Override
//	public Vehicle mapRow(ResultSet rs, int rowNum) throws SQLException {
//		Vehicle vehicle = new Vehicle();
//		vehicle.setCode(rs.getInt("code"));
//		vehicle.setName(rs.getString("name"));
//		vehicle.setRegister_no(rs.getString("register_no"));
//		vehicle.setModel(rs.getString("model"));
//		vehicle.setBranch(rs.getString("model"));
//		vehicle.setVersion(rs.getString("version"));
//		vehicle.setDescriptions(rs.getString("description"));
//		vehicle.setCondition(rs.getString("conditions"));
//		Integer codeUser = rs.getInt("code_user");
//		
//		String sql = "select * from user where code="+codeUser;
//		User user = (User) jdbcTemplate.query(sql, new UserRowMapper(jdbcTemplate));
//		vehicle.setCodeOwner(user);
//		
//		return vehicle;	
//	}
	@Override
	public Vehicle extractData(ResultSet rs) throws SQLException, DataAccessException {
		if (rs.next()) {
			Integer code = rs.getInt("code");
			String name = rs.getString("name");
			String registerNo = rs.getString("register_no");
			String model = rs.getString("model");
			String branch = rs.getString("branch");
			String version = rs.getString("version");
			String description = rs.getString("description");
			String condition = rs.getString("conditions");
			Integer codeUser = rs.getInt("code_user");
			
			String sql = "select * from user where code="+codeUser;
			User user = (User) jdbcTemplate.query(sql, new UserRowMapper(jdbcTemplate));
			
			return new Vehicle(code, name, registerNo, model, branch, version, description, condition, user);
		}
		return null;
	}

}
