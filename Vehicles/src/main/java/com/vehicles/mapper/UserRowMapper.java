package com.vehicles.mapper;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import com.vehicles.model.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserRowMapper implements ResultSetExtractor<User> {
	
	private JdbcTemplate jdbcTemplate;
	public UserRowMapper(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

//	@Override
//	public User mapRow(ResultSet rs, int rowNum) throws SQLException {
//		User user = new User();
//		user.setCode(rs.getInt("code"));
//		user.setName(rs.getString("name"));
//		user.setAddress(rs.getString("name"));
//		user.setIsCooperator(rs.getBoolean("is_cooperator"));
//		user.setAddress(rs.getString("address"));
//		user.setPhone(rs.getString("phone"));
//		user.setNote(rs.getString("note"));
//		
//		return user;
//	}

	@Override
	public User extractData(ResultSet rs) throws SQLException, DataAccessException {
		if (rs.next()) {
			Integer code = rs.getInt("code");
			String name = rs.getString("name");
			Boolean isCooperator = rs.getBoolean("is_cooperator");
			String address = rs.getString("address");
			String phone = rs.getString("phone");
			String note = rs.getString("note");
			
			return new User(code, name, isCooperator, address, phone, note);
		}
		return null;
	}

}
