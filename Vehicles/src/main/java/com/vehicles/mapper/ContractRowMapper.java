package com.vehicles.mapper;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import com.vehicles.model.Contract;
import com.vehicles.model.User;
import com.vehicles.model.Vehicle;

public class ContractRowMapper implements ResultSetExtractor<Contract> {

	
	private JdbcTemplate jdbcTemplate;
	public ContractRowMapper(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
//	@Override
//	public Contract mapRow(ResultSet rs, int rowNum) throws SQLException {
//		Contract contract = new Contract();
//		contract.setCode(rs.getInt("code"));
//		contract.setRentalDate(rs.getDate("rental_date"));
//		contract.setReturnDate(rs.getDate("return_date"));
//		contract.setSum(rs.getDouble("sum"));
//		contract.setDeposit(rs.getDouble("deposit"));
//		contract.setStatus(rs.getBoolean("status"));
//		contract.setType_contract(rs.getString("type_contract"));
//		contract.setAsset(rs.getString("asset"));
//		Integer codeUser = rs.getInt("code_user");
//		Integer codeVehicle = rs.getInt("code_vehicle");
//		
//		String sqlUser = "select * from user where code="+codeUser;
//		String sqlVehicle = "select * from vehicle where code="+codeVehicle;
//		
//		User user = jdbcTemplate.queryForObject(sqlUser, new UserRowMapper(jdbcTemplate));
//		Vehicle vehicle = jdbcTemplate.query(sqlVehicle, new VehicleRowMapper(jdbcTemplate));
//		
//		contract.setCodeUser(user);
//		contract.setCodeVehicle(vehicle);
//		
//		return contract;
//	}
	@Override
	public Contract extractData(ResultSet rs) throws SQLException, DataAccessException {
		if (rs.next()) {
			Integer code = rs.getInt("code");
			Date rentalDate = rs.getDate("rental_date");
			Date returnDate = rs.getDate("return_date");
			Double deposit = rs.getDouble("deposit");
			Boolean status = rs.getBoolean("status");
			String typeContract = rs.getString("type_contract");
			String asset = rs.getString("asset");
			
			Integer codeUser = rs.getInt("code_user");
			Integer codeVehicle = rs.getInt("code_vehicle");
				
			String sqlUser = "select * from user where code="+codeUser;
			String sqlVehicle = "select * from vehicle where code="+codeVehicle;
			
			User user = jdbcTemplate.query(sqlUser, new UserRowMapper(jdbcTemplate));
			Vehicle vehicle = jdbcTemplate.query(sqlVehicle, new VehicleRowMapper(jdbcTemplate));
			
			return new Contract(code, rentalDate, returnDate, deposit, status, typeContract, asset, user, vehicle);

		}
		return null;
	}

}
