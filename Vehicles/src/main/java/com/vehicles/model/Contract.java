package com.vehicles.model;

import java.sql.Date;
import java.util.List;

import com.vehicles.model.User;
import com.vehicles.model.Vehicle;

public class Contract {
	private Integer code;
	private Date rentalDate;
	private Date returnDate;
	private Double deposit;
	private Boolean status;
	private String type_contract;
	private String asset;
	private User codeUser;
	private Vehicle codeVehicle;
	
	public Contract() {
		
	}
	public Contract(Integer code, Date rentalDate, Date returnDate, Double deposit, Boolean status,
			String type_contract, String asset, User codeUser, Vehicle codeVehicle) {
		super();
		this.code = code;
		this.rentalDate = rentalDate;
		this.returnDate = returnDate;
		this.deposit = deposit;
		this.status = status;
		this.type_contract = type_contract;
		this.asset = asset;
		this.codeUser = codeUser;
		this.codeVehicle = codeVehicle;
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public Date getRentalDate() {
		return rentalDate;
	}
	public void setRentalDate(Date rentalDate) {
		this.rentalDate = rentalDate;
	}
	public Date getReturnDate() {
		return returnDate;
	}
	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}
	public Double getDeposit() {
		return deposit;
	}
	public void setDeposit(Double deposit) {
		this.deposit = deposit;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public String getType_contract() {
		return type_contract;
	}
	public void setType_contract(String type_contract) {
		this.type_contract = type_contract;
	}
	public String getAsset() {
		return asset;
	}
	public void setAsset(String asset) {
		this.asset = asset;
	}
	public User getCodeUser() {
		return codeUser;
	}
	public void setCodeUser(User codeUser) {
		this.codeUser = codeUser;
	}
	public Vehicle getCodeVehicle() {
		return codeVehicle;
	}
	public void setCodeVehicle(Vehicle codeVehicle) {
		this.codeVehicle = codeVehicle;
	}
	public Integer getIdUser() {
		return codeUser.getCode();
	}
	public Integer getIdVehicle() {
		return codeVehicle.getCode();
	}
}
