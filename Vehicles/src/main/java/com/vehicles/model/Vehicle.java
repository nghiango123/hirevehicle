package com.vehicles.model;

import com.vehicles.model.User;

public class Vehicle {
	private Integer code;
	private String name;
	private String register_no;
	private String model;
	private String branch;
	private String version;
	private String descriptions;
	private String condition;
	private User codeOwner;
	
	public Vehicle() {
		
	}
	
	public Vehicle(Integer code, String name, String register_no, String model, String branch, String version,
			String descriptions, String condition, User codeOwner) {
		super();
		this.code = code;
		this.name = name;
		this.register_no = register_no;
		this.model = model;
		this.branch = branch;
		this.version = version;
		this.descriptions = descriptions;
		this.condition = condition;
		this.codeOwner = codeOwner;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRegister_no() {
		return register_no;
	}

	public void setRegister_no(String register_no) {
		this.register_no = register_no;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getDescriptions() {
		return descriptions;
	}

	public void setDescriptions(String descriptions) {
		this.descriptions = descriptions;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public User getCodeOwner() {
		return codeOwner;
	}

	public void setCodeOwner(User codeOwner) {
		this.codeOwner = codeOwner;
	}
	
	public Integer getIdOwner() {
		return codeOwner.getCode();
	}
	
}
