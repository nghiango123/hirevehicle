package com.vehicles.model;

import java.util.Date;

public class User {
	private Integer code;
	private String name;
	private Boolean isCooperator;
	private String address;
	private String phone;
	private String note;
	
	public User() {
		
	}

	public User(String name, Boolean isCooperator, String address, String phone,
			String note) {
		super();
		this.name = name;
		this.isCooperator = isCooperator;
		this.address = address;
		this.phone = phone;
		this.note = note;
	}
	
	public User(Integer code, String name, Boolean isCooperator, String address, String phone,
			String note) {
		this(name, isCooperator, address, phone, note);
		this.code = code;

	}
	
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Boolean getIsCooperator() {
		return isCooperator;
	}
	public void setIsCooperator(Boolean isCooperator) {
		this.isCooperator = isCooperator;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
}
