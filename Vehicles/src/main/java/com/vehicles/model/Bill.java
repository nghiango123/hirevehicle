package com.vehicles.model;

import java.sql.Date;

import com.vehicles.model.Contract;
import com.vehicles.model.Staff;

public class Bill {
	private Integer code;
	private Double unit;
	private Double total;
	private String listBroken;
	private Double compensation;
	private Date datePay;
	private Contract codeContract;
	private Staff codeStaff;
	
	public Bill() {
		
	}
	public Bill(Contract codeContract) {
		this.codeContract = codeContract;
	}
	public Bill(Double unit, Double total, String listBroken, Double compensation, Contract codeContract,
			Staff codeStaff) {
		this.unit = unit;
		this.total = total;
		this.listBroken = listBroken;
		this.compensation = compensation;
		this.codeContract = codeContract;
		this.codeStaff = codeStaff;
	}
	public Bill(Double unit, Double total, String listBroken, Double compensation, Date datePay, Contract codeContract,
			Staff codeStaff) {
		this.unit = unit;
		this.total = total;
		this.listBroken = listBroken;
		this.compensation = compensation;
		this.datePay = datePay;
		this.codeContract = codeContract;
		this.codeStaff = codeStaff;
	}
	public Bill(Integer code, Double unit, Double total, String listBroken, Double compensation, Date datePay, Contract codeContract,
			Staff codeStaff) {
		super();
		this.code = code;
		this.unit = unit;
		this.total = total;
		this.listBroken = listBroken;
		this.compensation = compensation;
		this.datePay = datePay;
		this.codeContract = codeContract;
		this.codeStaff = codeStaff;
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public Double getUnit() {
		return unit;
	}
	public void setUnit(Double unit) {
		this.unit = unit;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public String getListBroken() {
		return listBroken;
	}
	public void setListBroken(String listBroken) {
		this.listBroken = listBroken;
	}
	public Double getCompensation() {
		return compensation;
	}
	public void setCompensation(Double compensation) {
		this.compensation = compensation;
	}
	public Date getDatePay() {
		return datePay;
	}
	public void setDatePay(Date datePay) {
		this.datePay = datePay;
	}
	public Contract getCodeContract() {
		return codeContract;
	}
	public void setCodeContract(Contract codeContract) {
		this.codeContract = codeContract;
	}
	public Staff getCodeStaff() {
		return codeStaff;
	}
	public void setCodeStaff(Staff codeStaff) {
		this.codeStaff = codeStaff;
	}
	public Integer getIdStaff() {
		return codeStaff.getCode();
	}
	public Integer getIdContract() {
		return codeContract.getCode();
	}
	
}
