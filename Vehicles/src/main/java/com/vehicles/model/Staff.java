package com.vehicles.model;

import java.util.Date;

public class Staff {
	private Integer code;
	private String name;
	private String address;
	private String phone;
	private String position;
	private String note;
	
	public Staff() {
		
	}

	public Staff(Integer code, String name, String address, String phone, String position, String note) {
		super();
		this.code = code;
		this.name = name;
		this.address = address;
		this.phone = phone;
		this.position = position;
		this.note = note;
	}

	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	
}
