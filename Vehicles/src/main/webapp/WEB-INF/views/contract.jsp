<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Contract</title>	
		<!-- Google Font: Source Sans Pro -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
		
		<!-- Data Table -->
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/plugins/datatables/datatables.min.css"/>">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="<c:url value="/resources/plugins/fontawesome-free/css/all.min.css"/>">
		<!-- Theme style -->
		<link rel="stylesheet" href="<c:url value="/resources/css/adminlte.min.css"/>">
	

	</head>
	<body class="hold-transition sidebar-mini">
		<div class="wrapper">
			<!-- Preloader -->
			<div class="preloader flex-column justify-content-center align-items-center">
				<img class="animation__shake" src="<c:url value="/resources/img/AdminLTELogo.png"/>" alt="AdminLTELogo" height="60" width="60">
			</div>
			<!-- Navbar -->
			<nav class="main-header navbar navbar-expand navbar-white navbar-light">
				<ul class="navbar-nav">
			      <li class="nav-item">
			        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
			      </li>
			      <li class="nav-item d-none d-sm-inline-block">
			        <a href="index3.html" class="nav-link">Home</a>
			      </li>
			      <li class="nav-item d-none d-sm-inline-block">
			        <a href="#" class="nav-link">Contact</a>
			      </li>
			    </ul>
			    <!-- Right navbar links -->		    
			    <ul class="navbar-nav ml-auto">
			      <!-- Navbar Search -->
			      <li class="nav-item">
			        <a class="nav-link" data-widget="navbar-search" href="#" role="button">
			          <i class="fas fa-search"></i>
			        </a>
			        <div class="navbar-search-block">
			          <form class="form-inline">
			            <div class="input-group input-group-sm">
			              <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
			              <div class="input-group-append">
			                <button class="btn btn-navbar" type="submit">
			                  <i class="fas fa-search"></i>
			                </button>
			                <button class="btn btn-navbar" type="button" data-widget="navbar-search">
			                  <i class="fas fa-times"></i>
			                </button>
			              </div>
			            </div>
			          </form>
			        </div>
			      </li>
			
			      <!-- Messages Dropdown Menu -->
			      <li class="nav-item dropdown">
			        <a class="nav-link" data-toggle="dropdown" href="#">
			          <i class="far fa-comments"></i>
			          <span class="badge badge-danger navbar-badge">3</span>
			        </a>
			        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
			          <a href="#" class="dropdown-item">
			            <!-- Message Start -->
			            <div class="media">
			              <img src="<c:url value="/resources/img/user1-128x128.jpg"/>" alt="User Avatar" class="img-size-50 mr-3 img-circle">
			              <div class="media-body">
			                <h3 class="dropdown-item-title">
			                  Brad Diesel
			                  <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
			                </h3>
			                <p class="text-sm">Call me whenever you can...</p>
			                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
			              </div>
			            </div>
			            <!-- Message End -->
			          </a>
			          <div class="dropdown-divider"></div>
			          <a href="#" class="dropdown-item">
			            <!-- Message Start -->
			            <div class="media">
			              <img src="<c:url value="/resources/img/user8-128x128.jpg"/>" alt="User Avatar" class="img-size-50 img-circle mr-3">
			              <div class="media-body">
			                <h3 class="dropdown-item-title">
			                  John Pierce
			                  <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
			                </h3>
			                <p class="text-sm">I got your message bro</p>
			                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
			              </div>
			            </div>
			            <!-- Message End -->
			          </a>
			          <div class="dropdown-divider"></div>
			          <a href="#" class="dropdown-item">
			            <!-- Message Start -->
			            <div class="media">
			              <img src="<c:url value="/resources/img/user3-128x128.jpg"/>" alt="User Avatar" class="img-size-50 img-circle mr-3">
			              <div class="media-body">
			                <h3 class="dropdown-item-title">
			                  Nora Silvester
			                  <span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>
			                </h3>
			                <p class="text-sm">The subject goes here</p>
			                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
			              </div>
			            </div>
			            <!-- Message End -->
			          </a>
			          <div class="dropdown-divider"></div>
			          <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
			        </div>
			      </li>
			      <!-- Notifications Dropdown Menu -->
			      <li class="nav-item dropdown">
			        <a class="nav-link" data-toggle="dropdown" href="#">
			          <i class="far fa-bell"></i>
			          <span class="badge badge-warning navbar-badge">15</span>
			        </a>
			        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
			          <span class="dropdown-item dropdown-header">15 Notifications</span>
			          <div class="dropdown-divider"></div>
			          <a href="#" class="dropdown-item">
			            <i class="fas fa-envelope mr-2"></i> 4 new messages
			            <span class="float-right text-muted text-sm">3 mins</span>
			          </a>
			          <div class="dropdown-divider"></div>
			          <a href="#" class="dropdown-item">
			            <i class="fas fa-users mr-2"></i> 8 friend requests
			            <span class="float-right text-muted text-sm">12 hours</span>
			          </a>
			          <div class="dropdown-divider"></div>
			          <a href="#" class="dropdown-item">
			            <i class="fas fa-file mr-2"></i> 3 new reports
			            <span class="float-right text-muted text-sm">2 days</span>
			          </a>
			          <div class="dropdown-divider"></div>
			          <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
			        </div>
			      </li>
			      <li class="nav-item">
			        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
			          <i class="fas fa-expand-arrows-alt"></i>
			        </a>
			      </li>
			      <li class="nav-item">
			        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
			          <i class="fas fa-th-large"></i>
			        </a>
			      </li>
			    </ul>
			</nav>
			<aside class="main-sidebar sidebar-dark-primary elevation-4">
			 	<!-- Brand Logo -->
			    <a href="#" class="brand-link">
			      <img src="<c:url value="/resources/img/AdminLTELogo.png"/>" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
			      <span class="brand-text font-weight-light">Quản lý thuê xe</span>
			    </a>
				<!-- Sidebar -->
				<div class="sidebar">
					<!-- Sidebar user panel (optional) -->
					<div class="user-panel mt-3 pb-3 mb-3 d-flex">
						<div class="image">
						  <img src="<c:url value="/resources/img/user2-160x160.jpg"/>" class="img-circle elevation-2" alt="User Image">
						</div>
						<div class="info">
						  <a href="#" class="d-block">Phạm Ngọc Ánh</a>
						</div>
					</div>
							    <!-- SidebarSearch Form -->
				<div class="form-inline">
				  <div class="input-group" data-widget="sidebar-search">
				    <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
				    <div class="input-group-append">
				      <button class="btn btn-sidebar">
				        <i class="fas fa-search fa-fw"></i>
				      </button>
				    </div>
				  </div>
				</div>
				<!-- Sidebar Menu -->
				<nav class="mt-2">
					<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
					  <!-- Add icons to the links using the .nav-icon class
	               	  	        with font-awesome or any other icon font library -->
			          <li class="nav-item">
			            <a href="#" class="nav-link">
			              <i class="nav-icon fas fa-tachometer-alt"></i>
			              <p>
			                Hợp đồng
			                <i class="right fas fa-angle-left"></i>
			              </p>
			            </a>
			            <ul class="nav nav-treeview">
			              <li class="nav-item">
			                <a href="#" class="nav-link">
			                  <i class="far fa-circle nav-icon"></i>
			                  <p>Hợp đồng thuê xe</p>
			                </a>
			              </li>
			              <li class="nav-item">
			                <a href="<c:url value="contractall"/>" class="nav-link">
			                  <i class="far fa-circle nav-icon"></i>
			                  <p>Hợp đồng ký gửi</p>
			                </a>
			              </li>
			              <li class="nav-item">
			                <a href="<c:url value="contract"/>" class="nav-link">
			                  <i class="far fa-circle nav-icon"></i>
			                  <p>Thanh toán HĐ ký gửi</p>
			                </a>
			              </li>
			               <li class="nav-item">
			                <a href="<c:url value="#"/>" class="nav-link">
			                  <i class="far fa-circle nav-icon"></i>
			                  <p>Thanh toán HĐ thuê xe</p>
			                 </a>
			               </li>
			            </ul>
			          </li>
					  <li class="nav-item">
			            <a href="#" class="nav-link">
			              <i class="nav-icon fas fa-copy"></i>
			              <p>
			                Xe
			                <i class="fas fa-angle-left right"></i>
			              </p>
			            </a>
			            <ul class="nav nav-treeview">
			              <li class="nav-item">
			                <a href="#" class="nav-link">
			                  <i class="far fa-circle nav-icon"></i>
			                  <p>Xe ô tô 4 chỗ</p>
			                </a>
			              </li>
			              <li class="nav-item">
			                <a href="#" class="nav-link">
			                  <i class="far fa-circle nav-icon"></i>
			                  <p>Xe ô tô 7 chỗ</p>
			                </a>
			              </li>
			              <li class="nav-item">
			                <a href="#" class="nav-link">
			                  <i class="far fa-circle nav-icon"></i>
			                  <p>Xe máy</p>
			                </a>
			              </li>
			              <li class="nav-item">
			                <a href="#" class="nav-link">
			                  <i class="far fa-circle nav-icon"></i>
			                  <p>Xe chở hàng</p>
			                </a>
			              </li>        
			            </ul>  
			          </li>
					  <li class="nav-item">
			            <a href="#" class="nav-link">
			              <i class="nav-icon fas fa-chart-pie"></i>
			              <p>
			                Khách hàng
			                <i class="right fas fa-angle-left"></i>
			              </p>
			            </a>
			            <ul class="nav nav-treeview">
			              <li class="nav-item">
			                <a href="#" class="nav-link">
			                  <i class="far fa-circle nav-icon"></i>
			                  <p>Người thuê xe</p>
			                </a>
			              </li>
			              <li class="nav-item">
			                <a href="#" class="nav-link">
			                  <i class="far fa-circle nav-icon"></i>
			                  <p>Đối tác</p>
			                </a>
			              </li>
			            </ul>
			          </li>
					  <li class="nav-item">
			            <a href="#" class="nav-link">
			              <i class="nav-icon fas fa-tree"></i>
			              <p>
			                Hóa đơn
			                <i class="fas fa-angle-left right"></i>
			              </p>
			            </a>
			            <ul class="nav nav-treeview">
			              <li class="nav-item">
			                <a href="#" class="nav-link">
			                  <i class="far fa-circle nav-icon"></i>
			                  <p>Tạo hóa đơn</p>
			                </a>
			              </li>
			              <li class="nav-item">
			                <a href="#" class="nav-link">
			                  <i class="far fa-circle nav-icon"></i>
			                  <p>Lịch sử</p>
			                </a>
			              </li>
			            </ul>
			          </li>
					  <li class="nav-item">
			            <a href="#" class="nav-link">
			              <i class="nav-icon fas fa-edit"></i>
			              <p>
			                Nhân viên
			                <i class="fas fa-angle-left right"></i>
			              </p>
			            </a>
			            <ul class="nav nav-treeview">
			              <li class="nav-item">
			                <a href="#" class="nav-link">
			                  <i class="far fa-circle nav-icon"></i>
			                  <p>Danh sách</p>
			                </a>
			              </li>
			              <li class="nav-item">
			                <a href="#" class="nav-link">
			                  <i class="far fa-circle nav-icon"></i>
			                  <p>Thêm mới</p>
			                </a>
			              </li>
			            </ul>
			          </li>
				   	  <li class="nav-header">Tiện ích</li>
					  <li class="nav-item">
			            <a href="#" class="nav-link">
			              <i class="nav-icon far fa-calendar-alt"></i>
			              <p>
			                Thời gian biểu
			                <span class="badge badge-info right">2</span>
			              </p>
			            </a>
			          </li>
				      <li class="nav-item">
			            <a href="#" class="nav-link">
			              <i class="nav-icon far fa-image"></i>
			              <p>
			                Thư viện
			              </p>
			            </a>
			          </li>
			          <li class="nav-item">
			            <a href="#" class="nav-link">
			              <i class="nav-icon far fa-image"></i>
			              <p>
			                Ghi chú
			              </p>
			            </a>
			          </li>
			          <li class="nav-item">
			            <a href="#" class="nav-link">
			              <i class="nav-icon fas fa-th"></i>
			              <p>
			                Tài liệu
			                <span class="right badge badge-danger">New</span>
			              </p>
			            </a>
			          </li>
					  <li class="nav-item">
			            <a href="#" class="nav-link">
			              <i class="nav-icon far fa-envelope"></i>
			              <p>
			                Hộp thư thoại
			                <i class="fas fa-angle-left right"></i>
			              </p>
			            </a>
			            <ul class="nav nav-treeview">
			              <li class="nav-item">
			                <a href="pages/mailbox/mailbox.html" class="nav-link">
			                  <i class="far fa-circle nav-icon"></i>
			                  <p>Inbox</p>
			                </a>
			              </li>
			              <li class="nav-item">
			                <a href="pages/mailbox/compose.html" class="nav-link">
			                  <i class="far fa-circle nav-icon"></i>
			                  <p>Compose</p>
			                </a>
			              </li>
			              <li class="nav-item">
			                <a href="pages/mailbox/read-mail.html" class="nav-link">
			                  <i class="far fa-circle nav-icon"></i>
			                  <p>Read</p>
			                </a>
			              </li>
			            </ul>
			          </li>
				
					  <li class="nav-item">
			            <a href="#" class="nav-link">
			              <i class="nav-icon fas fa-search"></i>
			              <p>
			                Tìm kiếm
			                <i class="fas fa-angle-left right"></i>
			              </p>
			            </a>
			            <ul class="nav nav-treeview">
			              <li class="nav-item">
			                <a href="pages/search/simple.html" class="nav-link">
			                  <i class="far fa-circle nav-icon"></i>
			                  <p>Simple Search</p>
			                </a>
			              </li>
			              <li class="nav-item">
			                <a href="pages/search/enhanced.html" class="nav-link">
			                  <i class="far fa-circle nav-icon"></i>
			                  <p>Enhanced</p>
			                </a>
			              </li>
			            </ul>
			          </li>
			     
				      <li class="nav-header">Tài khoản</li>
				      <li class="nav-item">
			            <a href="#" class="nav-link">
			              <i class="nav-icon far fa-circle text-info"></i>
			              <p>Thông tin cá nhân</p>
			            </a>
			          </li>
			          <li class="nav-item">
			            <a href="#" class="nav-link">
			              <i class="nav-icon far fa-circle text-danger"></i>
			              <p class="text">Thay đổi mật khẩu</p>
			            </a>
			          </li>
			          <li class="nav-item">
			            <a href="#" class="nav-link">
			              <i class="nav-icon far fa-circle text-warning"></i>
			              <p>Thoát đăng nhập</p>
			            </a>
			          </li>   
				   </ul>
				</nav>
			 </div>
	
			<!-- /.sidebar-menu -->
			</aside>
		 	<div class="content-wrapper">
			 	<section class="content-header">
			      <div class="container-fluid">
			        <div class="row mb-2">
			          <div class="col-sm-6">
			            <h1 class="m-0">Hợp đồng kí gửi </h1>
			          </div><!-- /.col -->
			          <div class="col-sm-6">
			            <ol class="breadcrumb float-sm-right">
			              <li class="breadcrumb-item"><a href="#">Home</a></li>
			              <li class="breadcrumb-item active">Contract</li>
			            </ol>
			          </div><!-- /.col -->
			        </div><!-- /.row -->
			      </div><!-- /.container-fluid -->
			    </section>
			    <section class="content">
			      <div class="container-fluid">
			        <div class="row">
			          <div class="col-12">
			            <div class="card">
			              <div class="card-header">
			                <h3 class="card-title">Danh sách hợp đồng chưa thanh toán</h3>
			              </div>
			              <div class="card-body">
			              	<table id="example1" class="table table-bordered table-hover" style="text-align: center">
			              	  <thead>
			                  <tr>
			                    <th>Mã</th>
			                    <th>Tên xe</th>
			                    <th>Tên đối tác</th>
			                    <th>Ngày nhận</th>
			                    <th>Ngày trả</th>
			                    <th>Trạng thái</th>
			                    <th>Thanh toán</th>
			                  </tr>
			                  </thead>
			                  <tbody>
			                  <c:forEach items="${contracts}" var="contract" varStatus="status">
			                  <tr>
			                  	<td>${contract.code }</td>
			                  	<td>${contract.codeVehicle.name }</td>
			                  	<td>${contract.codeUser.name }</td>
			                  	<td>${contract.rentalDate }</td>
			                  	<td>${contract.returnDate }</td>
			                  	<td><span class="${contract.status == 'true'? "badge badge-success":"badge badge-danger" }">${contract.status == "true" ? "Đã thanh toán":"Chưa thanh toán" }</span></td>
			                  	<td>
		                  	      <a id="payment" class="${contract.status == 'false' ? "btn btn-primary btn-sm":"btn btn-secondary btn-sm disabled"  }" href="<c:url value="bill?code=${contract.code }"/>">
		                  	    	${contract.status == "true" ? "Đã thanh toán":"Thanh toán" }
		                  	      </a>	            
			                  	</td>
			                  </tr>
			                  </c:forEach> 
			                  </tbody> 
			              	</table>
			              </div>	
			            </div>
			          </div>
			        </div>
			      </div>
			    </section>
		 	</div>
		 	<footer class="main-footer">
			    <div class="float-right d-none d-sm-block">
			      <b>Version</b> 3.1.0
			    </div>
		    	<strong>Copyright &copy; 2014-2021 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
		  	</footer>
		  	<aside class="control-sidebar control-sidebar-dark">
			    <!-- Control sidebar content goes here -->
			</aside>
		</div>
		

		
		
		
		<!-- jQuery -->
		<script src="<c:url value="/resources/plugins/jquery/jquery.min.js"/>"></script>
		<!-- Bootstrap 4 -->
		<script src="<c:url value="/resources/plugins/bootstrap/js/bootstrap.bundle.min.js"/>"></script>
		<!-- DataTables  & Plugins -->
		<script type="text/javascript" src="<c:url value="/resources/plugins/datatables/datatables.min.js"/>"></script>
		
<%-- 		<script src="<c:url value="/resources/plugins/jszip/jszip.min.js"/>"></script>
		<script src="<c:url value="/resources/plugins/pdfmake/pdfmake.min.js"/>"></script>
		<script src="<c:url value="/resources/plugins/pdfmake/vfs_fonts.js"/>"></script> --%>

		<!-- AdminLTE App -->
		<script src="<c:url value="/resources/js/adminlte.min.js"/>"></script>
		<script>
		  $(function () {
			    $("#example1").DataTable({
			      "responsive": true, "lengthChange": false, "autoWidth": false,
			      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
			    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
			  });
		</script>
	</body>
</html>