<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Bill</title>	

		<!-- Google Font: Source Sans Pro -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
		
		<!-- Data Table -->
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/plugins/datatables/datatables.min.css"/>">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="<c:url value="/resources/plugins/fontawesome-free/css/all.min.css"/>">
		<!-- Theme style -->
		<link rel="stylesheet" href="<c:url value="/resources/css/adminlte.min.css"/>">
	</head>
	<body class="hold-transition sidebar-mini">
	    <div class="wrapper">
			<!-- Preloader -->
			<div class="preloader flex-column justify-content-center align-items-center">
				<img class="animation__shake" src="<c:url value="/resources/img/AdminLTELogo.png"/>" alt="AdminLTELogo" height="60" width="60">
			</div>
			<!-- Navbar -->
			<nav class="main-header navbar navbar-expand navbar-white navbar-light">
				<ul class="navbar-nav">
			      <li class="nav-item">
			        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
			      </li>
			      <li class="nav-item d-none d-sm-inline-block">
			        <a href="index3.html" class="nav-link">Home</a>
			      </li>
			      <li class="nav-item d-none d-sm-inline-block">
			        <a href="#" class="nav-link">Contact</a>
			      </li>
			    </ul>
			    <!-- Right navbar links -->		    
			    <ul class="navbar-nav ml-auto">
			      <!-- Navbar Search -->
			      <li class="nav-item">
			        <a class="nav-link" data-widget="navbar-search" href="#" role="button">
			          <i class="fas fa-search"></i>
			        </a>
			        <div class="navbar-search-block">
			          <form class="form-inline">
			            <div class="input-group input-group-sm">
			              <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
			              <div class="input-group-append">
			                <button class="btn btn-navbar" type="submit">
			                  <i class="fas fa-search"></i>
			                </button>
			                <button class="btn btn-navbar" type="button" data-widget="navbar-search">
			                  <i class="fas fa-times"></i>
			                </button>
			              </div>
			            </div>
			          </form>
			        </div>
			      </li>
			
			      <!-- Messages Dropdown Menu -->
			      <li class="nav-item dropdown">
			        <a class="nav-link" data-toggle="dropdown" href="#">
			          <i class="far fa-comments"></i>
			          <span class="badge badge-danger navbar-badge">3</span>
			        </a>
			        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
			          <a href="#" class="dropdown-item">
			            <!-- Message Start -->
			            <div class="media">
			              <img src="<c:url value="/resources/img/user1-128x128.jpg"/>" alt="User Avatar" class="img-size-50 mr-3 img-circle">
			              <div class="media-body">
			                <h3 class="dropdown-item-title">
			                  Brad Diesel
			                  <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
			                </h3>
			                <p class="text-sm">Call me whenever you can...</p>
			                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
			              </div>
			            </div>
			            <!-- Message End -->
			          </a>
			          <div class="dropdown-divider"></div>
			          <a href="#" class="dropdown-item">
			            <!-- Message Start -->
			            <div class="media">
			              <img src="<c:url value="/resources/img/user8-128x128.jpg"/>" alt="User Avatar" class="img-size-50 img-circle mr-3">
			              <div class="media-body">
			                <h3 class="dropdown-item-title">
			                  John Pierce
			                  <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
			                </h3>
			                <p class="text-sm">I got your message bro</p>
			                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
			              </div>
			            </div>
			            <!-- Message End -->
			          </a>
			          <div class="dropdown-divider"></div>
			          <a href="#" class="dropdown-item">
			            <!-- Message Start -->
			            <div class="media">
			              <img src="<c:url value="/resources/img/user3-128x128.jpg"/>" alt="User Avatar" class="img-size-50 img-circle mr-3">
			              <div class="media-body">
			                <h3 class="dropdown-item-title">
			                  Nora Silvester
			                  <span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>
			                </h3>
			                <p class="text-sm">The subject goes here</p>
			                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
			              </div>
			            </div>
			            <!-- Message End -->
			          </a>
			          <div class="dropdown-divider"></div>
			          <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
			        </div>
			      </li>
			      <!-- Notifications Dropdown Menu -->
			      <li class="nav-item dropdown">
			        <a class="nav-link" data-toggle="dropdown" href="#">
			          <i class="far fa-bell"></i>
			          <span class="badge badge-warning navbar-badge">15</span>
			        </a>
			        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
			          <span class="dropdown-item dropdown-header">15 Notifications</span>
			          <div class="dropdown-divider"></div>
			          <a href="#" class="dropdown-item">
			            <i class="fas fa-envelope mr-2"></i> 4 new messages
			            <span class="float-right text-muted text-sm">3 mins</span>
			          </a>
			          <div class="dropdown-divider"></div>
			          <a href="#" class="dropdown-item">
			            <i class="fas fa-users mr-2"></i> 8 friend requests
			            <span class="float-right text-muted text-sm">12 hours</span>
			          </a>
			          <div class="dropdown-divider"></div>
			          <a href="#" class="dropdown-item">
			            <i class="fas fa-file mr-2"></i> 3 new reports
			            <span class="float-right text-muted text-sm">2 days</span>
			          </a>
			          <div class="dropdown-divider"></div>
			          <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
			        </div>
			      </li>
			      <li class="nav-item">
			        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
			          <i class="fas fa-expand-arrows-alt"></i>
			        </a>
			      </li>
			      <li class="nav-item">
			        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
			          <i class="fas fa-th-large"></i>
			        </a>
			      </li>
			    </ul>
			</nav>
			<aside class="main-sidebar sidebar-dark-primary elevation-4">
			 	<!-- Brand Logo -->
			    <a href="#" class="brand-link">
			      <img src="<c:url value="/resources/img/AdminLTELogo.png"/>" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
			      <span class="brand-text font-weight-light">Quản lý thuê xe</span>
			    </a>
				<!-- Sidebar -->
				<div class="sidebar">
					<!-- Sidebar user panel (optional) -->
					<div class="user-panel mt-3 pb-3 mb-3 d-flex">
						<div class="image">
						  <img src="<c:url value="/resources/img/user2-160x160.jpg"/>" class="img-circle elevation-2" alt="User Image">
						</div>
						<div class="info">
						  <a href="#" class="d-block">Phạm Ngọc Ánh</a>
						</div>
					</div>
							    <!-- SidebarSearch Form -->
				<div class="form-inline">
				  <div class="input-group" data-widget="sidebar-search">
				    <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
				    <div class="input-group-append">
				      <button class="btn btn-sidebar">
				        <i class="fas fa-search fa-fw"></i>
				      </button>
				    </div>
				  </div>
				</div>
				<!-- Sidebar Menu -->
				<nav class="mt-2">
					<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
					  <!-- Add icons to the links using the .nav-icon class
	               	  	        with font-awesome or any other icon font library -->
			          <li class="nav-item">
			            <a href="#" class="nav-link">
			              <i class="nav-icon fas fa-tachometer-alt"></i>
			              <p>
			                Hợp đồng
			                <i class="right fas fa-angle-left"></i>
			              </p>
			            </a>
			            <ul class="nav nav-treeview">
			              <li class="nav-item">
			                <a href="#" class="nav-link">
			                  <i class="far fa-circle nav-icon"></i>
			                  <p>Hợp đồng thuê xe</p>
			                </a>
			              </li>
			              <li class="nav-item">
			                <a href="<c:url value="contractall"/>" class="nav-link">
			                  <i class="far fa-circle nav-icon"></i>
			                  <p>Hợp đồng ký gửi</p>
			                </a>
			              </li>
			              <li class="nav-item">
			                <a href="<c:url value="contract"/>" class="nav-link">
			                  <i class="far fa-circle nav-icon"></i>
			                  <p>Thanh toán HĐ ký gửi</p>
			                </a>
			              </li>
			               <li class="nav-item">
			                <a href="<c:url value="#"/>" class="nav-link">
			                  <i class="far fa-circle nav-icon"></i>
			                  <p>Thanh toán HĐ thuê xe</p>
			                </a>
			              </li>
			            </ul>
			          </li>
					  <li class="nav-item">
			            <a href="#" class="nav-link">
			              <i class="nav-icon fas fa-copy"></i>
			              <p>
			                Xe
			                <i class="fas fa-angle-left right"></i>
			              </p>
			            </a>
			            <ul class="nav nav-treeview">
			              <li class="nav-item">
			                <a href="#" class="nav-link">
			                  <i class="far fa-circle nav-icon"></i>
			                  <p>Xe ô tô 4 chỗ</p>
			                </a>
			              </li>
			              <li class="nav-item">
			                <a href="#" class="nav-link">
			                  <i class="far fa-circle nav-icon"></i>
			                  <p>Xe ô tô 7 chỗ</p>
			                </a>
			              </li>
			              <li class="nav-item">
			                <a href="#" class="nav-link">
			                  <i class="far fa-circle nav-icon"></i>
			                  <p>Xe máy</p>
			                </a>
			              </li>
			              <li class="nav-item">
			                <a href="#" class="nav-link">
			                  <i class="far fa-circle nav-icon"></i>
			                  <p>Xe chở hàng</p>
			                </a>
			              </li>        
			            </ul>  
			          </li>
					  <li class="nav-item">
			            <a href="#" class="nav-link">
			              <i class="nav-icon fas fa-chart-pie"></i>
			              <p>
			                Khách hàng
			                <i class="right fas fa-angle-left"></i>
			              </p>
			            </a>
			            <ul class="nav nav-treeview">
			              <li class="nav-item">
			                <a href="#" class="nav-link">
			                  <i class="far fa-circle nav-icon"></i>
			                  <p>Người thuê xe</p>
			                </a>
			              </li>
			              <li class="nav-item">
			                <a href="#" class="nav-link">
			                  <i class="far fa-circle nav-icon"></i>
			                  <p>Đối tác</p>
			                </a>
			              </li>
			            </ul>
			          </li>
					  <li class="nav-item">
			            <a href="#" class="nav-link">
			              <i class="nav-icon fas fa-tree"></i>
			              <p>
			                Hóa đơn
			                <i class="fas fa-angle-left right"></i>
			              </p>
			            </a>
			            <ul class="nav nav-treeview">
			              <li class="nav-item">
			                <a href="#" class="nav-link">
			                  <i class="far fa-circle nav-icon"></i>
			                  <p>Tạo hóa đơn</p>
			                </a>
			              </li>
			              <li class="nav-item">
			                <a href="#" class="nav-link">
			                  <i class="far fa-circle nav-icon"></i>
			                  <p>Lịch sử</p>
			                </a>
			              </li>
			            </ul>
			          </li>
					  <li class="nav-item">
			            <a href="#" class="nav-link">
			              <i class="nav-icon fas fa-edit"></i>
			              <p>
			                Nhân viên
			                <i class="fas fa-angle-left right"></i>
			              </p>
			            </a>
			            <ul class="nav nav-treeview">
			              <li class="nav-item">
			                <a href="#" class="nav-link">
			                  <i class="far fa-circle nav-icon"></i>
			                  <p>Danh sách</p>
			                </a>
			              </li>
			              <li class="nav-item">
			                <a href="#" class="nav-link">
			                  <i class="far fa-circle nav-icon"></i>
			                  <p>Thêm mới</p>
			                </a>
			              </li>
			            </ul>
			          </li>
				   	  <li class="nav-header">Tiện ích</li>
					  <li class="nav-item">
			            <a href="#" class="nav-link">
			              <i class="nav-icon far fa-calendar-alt"></i>
			              <p>
			                Thời gian biểu
			                <span class="badge badge-info right">2</span>
			              </p>
			            </a>
			          </li>
				      <li class="nav-item">
			            <a href="#" class="nav-link">
			              <i class="nav-icon far fa-image"></i>
			              <p>
			                Thư viện
			              </p>
			            </a>
			          </li>
			          <li class="nav-item">
			            <a href="#" class="nav-link">
			              <i class="nav-icon far fa-image"></i>
			              <p>
			                Ghi chú
			              </p>
			            </a>
			          </li>
			          <li class="nav-item">
			            <a href="#" class="nav-link">
			              <i class="nav-icon fas fa-th"></i>
			              <p>
			                Tài liệu
			                <span class="right badge badge-danger">New</span>
			              </p>
			            </a>
			          </li>
					  <li class="nav-item">
			            <a href="#" class="nav-link">
			              <i class="nav-icon far fa-envelope"></i>
			              <p>
			                Hộp thư thoại
			                <i class="fas fa-angle-left right"></i>
			              </p>
			            </a>
			            <ul class="nav nav-treeview">
			              <li class="nav-item">
			                <a href="pages/mailbox/mailbox.html" class="nav-link">
			                  <i class="far fa-circle nav-icon"></i>
			                  <p>Inbox</p>
			                </a>
			              </li>
			              <li class="nav-item">
			                <a href="pages/mailbox/compose.html" class="nav-link">
			                  <i class="far fa-circle nav-icon"></i>
			                  <p>Compose</p>
			                </a>
			              </li>
			              <li class="nav-item">
			                <a href="pages/mailbox/read-mail.html" class="nav-link">
			                  <i class="far fa-circle nav-icon"></i>
			                  <p>Read</p>
			                </a>
			              </li>
			            </ul>
			          </li>
				
					  <li class="nav-item">
			            <a href="#" class="nav-link">
			              <i class="nav-icon fas fa-search"></i>
			              <p>
			                Tìm kiếm
			                <i class="fas fa-angle-left right"></i>
			              </p>
			            </a>
			            <ul class="nav nav-treeview">
			              <li class="nav-item">
			                <a href="pages/search/simple.html" class="nav-link">
			                  <i class="far fa-circle nav-icon"></i>
			                  <p>Simple Search</p>
			                </a>
			              </li>
			              <li class="nav-item">
			                <a href="pages/search/enhanced.html" class="nav-link">
			                  <i class="far fa-circle nav-icon"></i>
			                  <p>Enhanced</p>
			                </a>
			              </li>
			            </ul>
			          </li>
			     
				      <li class="nav-header">Tài khoản</li>
				      <li class="nav-item">
			            <a href="#" class="nav-link">
			              <i class="nav-icon far fa-circle text-info"></i>
			              <p>Thông tin cá nhân</p>
			            </a>
			          </li>
			          <li class="nav-item">
			            <a href="#" class="nav-link">
			              <i class="nav-icon far fa-circle text-danger"></i>
			              <p class="text">Thay đổi mật khẩu</p>
			            </a>
			          </li>
			          <li class="nav-item">
			            <a href="#" class="nav-link">
			              <i class="nav-icon far fa-circle text-warning"></i>
			              <p>Thoát đăng nhập</p>
			            </a>
			          </li>   
				   </ul>
				</nav>
			 </div>
	
			<!-- /.sidebar-menu -->
			</aside>
		 	<div class="content-wrapper">
			 	<section class="content-header">
			      <div class="container-fluid">
			        <div class="row mb-2">
			          <div class="col-sm-6">
			            <h1 class="m-0">Hóa đơn</h1>
			          </div><!-- /.col -->
			          <div class="col-sm-6">
			            <ol class="breadcrumb float-sm-right">
			              <li class="breadcrumb-item"><a href="#">Home</a></li>
			              <li class="breadcrumb-item active">Invoice</li>
			            </ol>
			          </div><!-- /.col -->
			        </div><!-- /.row -->
			      </div><!-- /.container-fluid -->
			    </section>
			    <section class="content">
			      <div class="container-fluid">
			        <div class="row">
			          <div class="col-12">
			            <div class="callout callout-info">
			              <h5><i class="fas fa-info"></i> Lưu ý:</h5>
			              Hệ thống đã có bản cập nhật mới. Hãy cùng trải nghiệm nào
			            </div>
			            <div class="invoice p-3 mb-3">
			              <div class="row">
			                <div class="col-12">
			                  <h4>
			                    <i class="fas fa-globe"></i> Hire Vehicle, Inc.
			                    <small class="float-right">
					                <div class="input-group mb-3">
					                  <div class="input-group-prepend">
					                    <button type="button" class="btn btn-danger">Mã hợp đồng</button>
					                  </div>
					                  <!-- /btn-group -->
					                  <input type="text" class="form-control" value="${contract.code }" name="codeContract">
					                </div>
			                    </small>
			                     
			                  </h4>
			                </div>
			              </div>
			              <div class="row invoice-info">
			              	<div class="col-sm-4 invoice-col">
			              	  From
			              	  <address>
			              	     <strong>Hire Vehicle, Inc.</strong><br>
			                     999 Trần Đại Nghĩa, Bách Khoa<br>
			                     Hai Bà Trưng, Hà Nội<br></br>
			                     Phone: (+84) 122-345-678<br>
			                     Email: info@almasaeedstudio.com
			              	  </address>
		              	    </div>
		              	    <div class="col-sm-4 invoice-col">
			                  To
			                  <address>
			                    <strong>${contract.codeUser.name }</strong><br>
			                    Địa chỉ: ${contract.codeUser.address }<br>
			                    Phone: (+84) ${contract.codeUser.phone }<br>
			                    Email: ngocanh@gmail.com
			                  </address>
			                </div>
			                <div class="col-sm-4 invoice-col">
			                  <b>Thông tin xe</b><br>
			                  <br>
			                  <b>Tên xe: </b> ${contract.codeVehicle.name }<br>
			                  <b>Biển số:</b> ${contract.codeVehicle.register_no }<br>
			                  <b>Hãng:</b> ${contract.codeVehicle.branch }<br>
			                  <b>Mô tả: </b> ${contract.codeVehicle.descriptions } 
			                </div>
			              </div>
			              <div class="row">
			                <h5>Thông tin hợp đồng</h5>
			              	<div class="col-12 table-responsive">
			              	  <table class="table table-striped">
			              	  	<thead>
			                    <tr>
			                      <th>STT</th>
			                      <th>Tên</th>
			                      <th>Nội dung</th>
			                      <th>Ghi chú</th>
			                    </tr>
			                    </thead>
			                    <tbody>
			                    <tr>
			                      <td>1</td>
			                      <td>Tài sản thế chấp</td>
			                      <td>${contract.asset }</td>
			                      <td>Còn nguyên vẹn</td>
			                    </tr>
			                    <tr>
			                      <td>2</td>
			                      <td>Ngày ký hợp đồng</td>
			                      <td>${contract.rentalDate }</td>			                      
			                      <td>Ngày </td>
			                    </tr>
			                    <tr>
			                      <td>3</td>
			                      <td>Ngày thanh toán</td>
			                      <td>${contract.returnDate }</td>
			                      <td>a</td>
			                    <tr>
			                    <tr>
			                      <td>4</td>
			                      <td>Xe đối tác</td>
			                      <td>${contract.codeVehicle.name } - ${contract.codeVehicle.register_no }</td>
			                      <td>${bill.listBroken }</td>
			                    </tr>			              
			                    </tbody>
			              	  </table>
			              	</div>
			              </div>
			              <div class="row">
			                <h5>Bảng đơn giá</h5>
			              	<div class="col-12 table-responsive">
			              	  <table class="table table-striped">
			              	  	<thead>
			                    <tr>
			                      <th>STT</th>
			                      <th>Tên</th>
			                      <th>Mô tả</th>
			                      <th>Subtotal</th>
			                    </tr>
			                    </thead>
			                    <tbody>
			                    <tr>
			                      <td>1</td>
			                      <td>Đơn giá theo ngày ($)</td>
			                      <td>Giá tiền thuê xe</td>
			                      <td>${payHire }</td>
			                    </tr>
			                    <tr>
			                      <td>2</td>
			                      <td>Danh sách hỏng hóc</td>
			                      <td>${bill.listBroken }</td>			                      
			                      <td>${bill.compensation } </td>
			                    </tr>		              
			                    </tbody>
			              	  </table>
			              	</div>
			              </div>
			            </div>
			          </div>
			        </div>
			   <div class="row">
                <!-- accepted payments column -->
                <div class="col-6">
                  <p class="lead">Payment Methods:</p>
                  <img src="<c:url value="/resources/img/credit/visa.png"/>" alt="Visa">
                  <img src="<c:url value="/resources/img/credit/mastercard.png"/>" alt="Mastercard">
                  <img src="<c:url value="/resources/img/credit/american-express.png"/>" alt="American Express">
                  <img src="<c:url value="/resources/img/credit/paypal2.png"/>" alt="Paypal">

                  <p class="text-muted well well-sm shadow-none" style="margin-top: 10px;">
                    Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem
                    plugg
                    dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
                  </p>
                </div>
                <!-- /.col -->
                <div class="col-6">
                  <p class="lead">Thanh toán ngày ${date }</p>

                  <div class="table-responsive">
                    <table class="table">
                      <tr>
                        <th style="width:50%">Subtotal:</th>
                        <td>${subTotal } $</td>
                      </tr>
                      <tr>
                        <th>Chiết khấu (1%)</th>
                        <td>${tax }</td>
                      </tr>
                      <tr>
                        <th>Total:</th>
                        <td>${total } $</td>
                      </tr>
                    </table>
                  </div>
                </div>
                <!-- /.col -->
              </div>
              
              <div class="row no-print">
                <div class="col-12">
                   <a href="#" rel="noopener" target="_blank" class="btn btn-default btn-sm"><i class="fas fa-print"></i> Print</a>
	               <a id="payment" class="btn btn-success btn-sm float-right" href="<c:url value="contract?code=${contract.code }"/>"><i class="far fa-credit-card"></i> Submit Payment</a>		                  	    
                  <button type="button" class="btn btn-primary float-right" style="margin-right: 5px;">
                    <i class="fas fa-download"></i> Generate PDF
                  </button>
                </div>
              </div>
			</div>
		  </section>
		 	</div>
		 	<footer class="main-footer">
			    <div class="float-right d-none d-sm-block">
			      <b>Version</b> 3.1.0
			    </div>
		    	<strong>Copyright &copy; 2014-2021 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
		  	</footer>
		  	<aside class="control-sidebar control-sidebar-dark">
			    <!-- Control sidebar content goes here -->
			</aside>
			
		</div>
		

		
		<!-- jQuery -->
		<script src="<c:url value="/resources/plugins/jquery/jquery.min.js"/>"></script>
		<!-- Bootstrap 4 -->
		<script src="<c:url value="/resources/plugins/bootstrap/js/bootstrap.bundle.min.js"/>"></script>
		<!-- DataTables  & Plugins -->
		<script type="text/javascript" src="<c:url value="/resources/plugins/datatables/datatables.min.js"/>"></script>
		<!-- AdminLTE App -->
		<script src="<c:url value="/resources/js/adminlte.min.js"/>"></script>

	</body>
</html>